/*! \file indexsort.cpp
 * \brief Functions for sorting a distance array and an index array. 
*/
#include <vector>
#include <algorithm>

//! Compair operator to compair the first int between <int, int> pairs
struct i_compair 
{
	bool operator () (const std::pair<int,int>& left, const std::pair<int,int>& right)
	{
		return left.first < right.first;
	}
};

//! Compair operator to compair the first double between <double, int> pairs
struct d_compair 
{
	bool operator () (const std::pair<double,int>& left, const std::pair<double,int>& right)
	{
		return left.first < right.first;
	}
};

//! Sort an index array based on a distance array of the same size.
/*!
 * \param distance Array of distances
 * \param index Secondary index array
 * \param size Size if the arrays
*/
void indexsort(int distance[], int index[], int size)
{
	std::vector<std::pair<int,int> > vec;


	for(int i=0;i<size;i++) 
		vec.push_back(std::pair<int,int>(distance[i],index[i]));
	

	std::sort(vec.begin(), vec.end(), i_compair()); 

	for(int i=0;i<size;i++) {
		distance[i] = vec[i].first;
		index[i] = vec[i].second; 
	}
}


//! Sort an index array based on a distance array of the same size.
/*!
 * \param distance Array of distances
 * \param index Secondary index array
 * \param size Size if the arrays
*/
void indexsort(double distance[], int index[], int size)
{
	std::vector<std::pair<double,int> > vec;


	for(int i=0;i<size;i++) 
		vec.push_back(std::pair<double,int>(distance[i],index[i]));
	

	std::sort(vec.begin(), vec.end(), d_compair()); 

	for(int i=0;i<size;i++) {
		distance[i] = vec[i].first;
		index[i] = vec[i].second; 
	}
}
