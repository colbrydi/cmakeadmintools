#include "indexsort.h" 
#include <stdlib.h>
#include <stdio.h>
 
int main(void)
{
   int rad[10];
   int ind[10];

   for(int i = 0;i<10;i++) {
	rad[i] = (int) rand() % 100;
	ind[i] = i;
	printf("\t%d",rad[i]);
   }
    printf("\n");
    for(int i=0;i<10;i++)  
	printf("\t%d",ind[i]);

    printf("\nTesting sorting function\n");
	// reads in data tables from text files
    indexsort(rad, ind, 10);
	
    for(int i=0;i<10;i++)  
	printf("\t%d",rad[i]);

    printf("\n");

    for(int i=0;i<10;i++)  
	printf("\t%d",ind[i]);

    printf("\n");
}

