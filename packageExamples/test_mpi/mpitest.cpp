/*
 * How to run assuming openmpi:
 * # wwlist > ~/nodes
 * # mpirun -np ?? -hostfile ~/nodes ./mpitest
*/

#include <stdio.h>          /* Needed for printf'ing */
#include <stdlib.h>
#include <mpi.h>            /* Get the MPI functions from this header */
#include <string.h>

#define max_nodes 264       /* Max number of nodes that we should test */
#define str_length 256       /* Largest string that can be used for hostnames */

const int TAG_PASS_NP=0;
const int TAG_START_NEXT_ITERATION=1;
const int TAG_TASKS_COMPLETE=2;

int iterations;

void usage(int argc, char ** argv)
{
     for (int i = 1; i < argc; i++) { 
	printf("parsing %d - %s\n",i, argv[i]);
        if (strcmp(argv[i],"-i")==0) {
		i++;
		iterations = atoi(argv[i]);
                printf("Setting iterations to %d\n",iterations);
	}
    }
	return;
}


int main(int argc, char **argv)
{
   /* Declare variables */
   int   proc, rank, size, namelen;
   int   ids[max_nodes];
   char  msgbuffer[str_length];
   char	 sendmsg[str_length];
   int	taskmsg[2];
 
   MPI_Status status;
   MPI_Init(&argc, &argv);
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   MPI_Comm_size(MPI_COMM_WORLD, &size);

   iterations = 1;
   usage(argc, argv);
  
   int np_msg[2][max_nodes];  
   
   for(int i = 0;i<size;i++) {
	np_msg[0][i] = -1;
	np_msg[1][i] = -1;
   }

   const int main = 0;
   int right = rank+1;
   int left = rank-1;
   int farright = size-1; 
   MPI_Request request;
   
   MPI_Barrier(MPI_COMM_WORLD);

   //TODO Put node inicliazaiton code here    // calculates the global variables the depend on simulation parameters
//    CalculateParameterDependentGlobalVariables();
	
	// reads in data tables from text files
//	ReadInAndGenerateDataTables();
 

   //TODO figure out what a nodes specific task list will look like.

   for(int iteration=0;iteration<iterations;iteration++)
   {
	for(int i = 0;i<size;i++) { 
        	np_msg[0][i] = -1;
	        np_msg[1][i] = -1;
   	}   
   	bool done=false;
   	bool active=false; // used in PART II
	bool tempdone=false;

	int np_sent=0;
	int np_recv=0;
	
	int task=0;
	int ntasks=10;
	//PART I - Simulation
	while(task < ntasks || !done) {
		if(task < ntasks) {
			int move = 0;
			//TODO PUT main node simulation here
			//move = simulate(task);
			printf("%d Simulating task %d\n", rank, task);
			if(task<rank && rank != farright) {
				np_sent++;
				move = 1;
				printf("moving np from %d to %d\n",rank,right );
			}

			//TODO Example to send a message to move the np to the right or "left"
			if( move != 0) {
				printf("%d sending np message\n", rank);
				sprintf(sendmsg,"np_message");				 
				MPI_Send(sendmsg,strlen(sendmsg)+1,MPI_INT,right,TAG_PASS_NP,MPI_COMM_WORLD);
			}
			task++;
		} else { //ALL DONE with simulation. 
			//Check to see if the tasks are done
			if(rank==main) { //mainnode
				//CHeck to see if all of the nodes are done with their simulation
				//printf("%d Checking done\n", rank);
				int tot_np_sent = 0;
				int tot_np_recv = 0;
				bool allrecv = true;
				for(int i=0;i<size;i++) {
					//printf("%d\tsent=%d\trecv=%d\n", i, np_msg[0][i],np_msg[1][i]);
					if(np_msg[0][i]==-1 || np_msg[1][i]==-1) {
						allrecv = false;
					} else {
						printf("From %i - %d sent %d recv\n", i, np_msg[0][i], np_msg[1][i]);
						tot_np_sent += np_msg[0][i];
						tot_np_recv += np_msg[1][i]; 
					}
					
				}
				printf("Checking messages %d = %d\n", tot_np_sent, tot_np_recv);
				if(tot_np_sent == tot_np_recv && allrecv) {
					sprintf(sendmsg, "next");
					printf("%d sending next\n", rank);
					for(int n=0;n < size;n++) 
						MPI_Send(sendmsg,strlen(sendmsg)+1,MPI_INT,n,TAG_START_NEXT_ITERATION,MPI_COMM_WORLD);
					done = true;
					active = true;
				}
			} 
			if(!tempdone) {
				printf("%d sending done_I, sent = %d recv = %d\n",rank,np_sent, np_recv);
				sprintf(sendmsg, "done_I");
				taskmsg[0] = np_sent;
				taskmsg[1] = np_recv;
				MPI_Send(taskmsg, sizeof(int)*2,MPI_INT,main,TAG_TASKS_COMPLETE,MPI_COMM_WORLD);
				tempdone = true;
			}
		}
		int checkmsg;
		MPI_Status status;
		MPI_Iprobe(MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&checkmsg,&status);
		while(checkmsg) {
			switch (status.MPI_TAG) {
				case TAG_START_NEXT_ITERATION: // From Main
					MPI_Recv(msgbuffer,sizeof(msgbuffer),MPI_BYTE,MPI_ANY_SOURCE,TAG_START_NEXT_ITERATION,MPI_COMM_WORLD,&status);
					printf("I'm %d DONE\n",rank);
					done = 1;
					break;
				case TAG_TASKS_COMPLETE:
					printf("%d Complete message from %d\n", rank, status.MPI_SOURCE);
					if(rank != main) 
						printf("ERROR: Complete message not recieved by main!");
					MPI_Recv(taskmsg,sizeof(int)*2,MPI_INT,MPI_ANY_SOURCE,TAG_TASKS_COMPLETE,MPI_COMM_WORLD,&status);
					np_msg[0][status.MPI_SOURCE] = taskmsg[0];
					np_msg[1][status.MPI_SOURCE] = taskmsg[1];
					break;
				case TAG_PASS_NP:
					MPI_Recv(msgbuffer,sizeof(msgbuffer),MPI_BYTE,MPI_ANY_SOURCE,TAG_PASS_NP,MPI_COMM_WORLD,&status);
					np_recv++;
					//add task to list
					tempdone = false;
					ntasks++;
					break;
				default:
					MPI_Recv(msgbuffer,sizeof(msgbuffer),MPI_BYTE,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
					printf("ERROR - unknown message type %d\n", status.MPI_TAG);
					
			}	
			MPI_Iprobe(MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&checkmsg,&status);
			//printf("%d Message from %d with Tag %d\n", rank, status.MPI_SOURCE, status.MPI_TAG);
		}
	}
	printf("Iteration Done %d of %d\n",iteration,iterations);
   	MPI_Barrier(MPI_COMM_WORLD);
   }
   MPI_Barrier(MPI_COMM_WORLD);
   if(rank == main)
	   printf("ALL DONE\n");
   MPI_Finalize();
   return(0);
}


