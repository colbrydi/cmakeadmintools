# Cmake makefile
# 
# This makefile is designed to work with cmake projects.  Put the
# makefile in the top level source directory.  It will automatically
# make a build directory and run cmake in the directory.
# The makefile also will work with clean, doxygen and test if they 
# are available in the project.
#
# Written by Dirk Colbry
# Copywrite 2010 MSU Board of Trustees. All rights reserved. 

all: build make 

install: build/Makefile
	(cd build; make install)

test: build/Makefile 
	(cd build; make test)

make: build/Makefile 
	(cd build; make)

build/Makefile: build
	(cd build; cmake .. 1> cmake.out 2> cmake.err; cat cmake.err | grep FOUND)

build:   
	mkdir build

dox: config.dox
	doxygen config.dox
	firefox ./html/index.html &

clean:
	rm -f  -r html 
	rm -f  -r build 
